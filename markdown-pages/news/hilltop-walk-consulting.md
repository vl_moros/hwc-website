---
title: Hilltop Walk Consulting
slug: hilltop-walk-consulting
author: Allan Guild
date: 2020-08-03T18:29:18.027Z
displayDate: Aug 2020
image: /images/HTW7.png
type: News
---

I am delighted to be able to share that today, Hilltop Walk Consulting has opened its doors for business.

It has been a bit of a leap of faith to start a new business in the midst of a global pandemic, and I am grateful for the support of friends and family, and in particular the clients and prospective clients who have, over the past several months, helped to shape the business.

> ## "The focus now is on clients and helping them to grow their businesses"

I have been fortunate to have spent 15 years working for two great investment banks in Goldman Sachs and HSBC, and to have had the opportunity to perform so many different roles in running electronic businesses, technology and operations teams and different change programmes.

I hope that I can now use those experiences to build a successful consulting business and to help the clients of Hilltop Walk Consulting achieve their goals.
