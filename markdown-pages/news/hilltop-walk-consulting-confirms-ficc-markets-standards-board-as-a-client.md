---
title: Hilltop Walk Consulting Confirms FMSB as a Client
author: Allan Guild
date: 2020-11-10T10:46:54.522Z
displayDate: Nov 2020
image: /images/fmsb1.png
slug: Hilltop-Walk-Consulting-Confirms-FICC-Markets-Standards-Board-as-a-Client
type: News
---

Hilltop Walk Consulting is pleased to confirm FICC Markets Standards Board (FMSB) as the first client of this new financial markets consultancy firm specialising in market conduct, strategy, technology and change. Hilltop Walk Consulting will be working with FMSB and its members to develop market Standards, Statements of Good Practice and Spotlight Reviews, in support of FMSB’s mission to ensure that wholesale FICC markets are transparent, fair and effective for all participants.

**Allan Guild, founder & Director of Hilltop Walk Consulting, said:** “I am excited to partner with FMSB and look forward to supporting them in their strategic goals. I can think of no better first client for Hilltop Walk Consulting than a member organisation with the standing of FMSB and we are delighted to be working with them to help them pursue their goal to raise standards of conduct in global FICC markets”

**Martin Pluves, CEO of FMSB, said:** “FMSB is delighted to appoint Hilltop Walk Consulting and to engage Allan Guild as an advisor on regulatory trading protocols and transparency in OTC markets. The relevance and extent of Allan’s experience in financial markets is well aligned to support the goals of FMSB and our members. We look forward to working with Hilltop Walk Consulting as we collectively undertake to develop Standards, Statements of Good Practice and Spotlight Reviews, to improve transparency in trading practices, and strengthen trust in global wholesale FICC markets by raising overall standards of conduct.”

>
