---
title: Managing Conduct Risk
slug: managing-conduct-risk
author: Allan Guild
date: 2021-10-18T06:45:01.229Z
displayDate: Oct 2021
image: /images/HTW2.png
type: WhitePaper
---
Over the ten years that have followed the financial crisis and the scandals in that time, there has been an increasing interest in conduct in financial markets. More specifically, an interest in the conduct of the practitioners, particularly in the big banks, who make the decisions around trading and selling financial products to customers. We have seen a number of individuals demonised and questions asked around how bad conduct was allowed to occur.

There are two things on which there seems to be a consensus on with regards to conduct in financial markets:

1. There are conflicts of interest in OTC financial markets in that an individual making a trading decision is likely to be faced with conflicting interests that they need to manage, eg between their own individual interest, the interests of their employee, particularly a sell-side bank and the interests of the customer with whom they are doing the trade
2. There is a long history of individuals an mismanaging these conflicts and committing a misconduct in a way that benefits the individual directly or their employer at the expense of others and in particular their customers.

A common notion over the past few years in big financial market participants has been the idea that bad conduct arises from bad people doing bad things – an approach commonly known as the bad apple theory.

If you agree with the bad apple theory then the natural solution to bad conduct is that you try and do two things:

1. Filter out the bad people
2. Put a control framework around decision-makers that removes their discretion to do something that would be bad conduct.

However, we are increasingly seeing that this approach doesn't really work.

It’s not possible to create a prescriptive approach that deals with the broad range of potential conflicts of interest that you can see within OTC markets. In these markets, liquidity providers and their customers benefit from highly skilled traders making informed decisions to facilitate customers’ access to the financial instruments that they need to support their underlying businesses. An overly prescriptive approach runs the risk of the unintended consequence of restricting that decision making ability in a way that causes detriment to customers.

It’s also simplistic to suggest that bad conduct comes from the action of a small group of people predisposed to engage in bad conduct. Studies of human behaviour have shown that if you can justify to yourself at particular course of action then you are likely to take that particular course of action. Therefore, it’s not necessarily a case of finding the bad people but instead about ensuring that you have an environment that empowers the people to make decisions and educates and incentivises them to make the right decisions when faced with potential conflicts of interest.