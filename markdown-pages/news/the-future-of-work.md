---
title: The Future of Work
slug: the-future-of-work
author: Allan Guild
date: 2021-06-07T06:45:01.229Z
displayDate: Jun 2021
image: /images/HTW4.png
type: WhitePaper
---

- The near-term return to the office is not the same as the longer-term future of work.
- The office might not be ready yet for a flexible or hybrid future of work.
- There won’t be a single-step process, or one-size-fits-all answer.
- Decision-makers and individual members of staff need to consider the limitations of a completely flexible or hybrid model.

---

As social distancing restrictions begin to ease, at Hilltop Walk Consulting, we have been in conversations with our clients, our families, our friends and each other on tackling the next challenge posed by the COVID-19 pandemic: the future state of work. Two things stand out from those conversations: The successful establishment of a secure and balanced ‘new normal’ is paramount and, in the short term, the return to the office is a separate first hurdle we need to overcome.

Remote working has changed much of how regulatory; compliance; and control obligations are upheld. The short-term mission of returning to the Remote working has changed so much of how we work and interact with both our work colleagues and the people we live with. There have been both benefits, in many cases of increased productivity and people spending more time with their family, and the damaging professional impact in terms of isolation from colleagues and fewer opportunities to learn and progress. The return to the office is an opportunity to address those damaging impacts and for each of us to reacquaint with the idea of working with others in person. Through this next phase, employers need to be sensitive to the impact of the last year on each member of staff and to not lose all the benefits that some people have experienced.

The future of work is different to the return to the office. Adopting a more flexible working environment, such as a hybrid model, as the ‘new normal’ would seem to be increasingly popular. 80% of the workforce said they would rather work at home for at least 3 days a week, than return to the office full time[1]. The office is likely to become a space for more collaborative tasks which are difficult to do virtually. This means that the way in which the office itself is used is likely to change. The limitations to complete flexibility in a Hybrid model need to be considered. For example: a team of 5 people will not necessarily want or need 2 desks per day, rather 5 desks on some days when they are all in the office to work together, and 1-3 desks on some days. This will create some challenging organisational and administrative burdens. Middle managers who determine that their team can work flexibly are likely to face pressure from corporate real estate around desk numbers and requirements, particularly as many firms have been discussing cutting office space by 20-40% to save on real estate costs[2]. Individual members of staff need to consider that there might not always be a desk available for them on days that they want to work in the office, and that with the office being used for collaboration, the days on which they are in the office might be determined by their colleagues and managers rather than by themselves.

We also need to consider that the office might not be ready for a flexible or hybrid future of work. When we left offices in March 2020, in most cases offices were setup with each individual member of staff having an assigned desk, with an expectation that the majority of their time in the office would be spent sat at that desk progressing their individual tasks. If the office becomes a place for more collaborative tasks that are difficult to do virtually, then the office needs to be setup differently with a layout that is more focused on discussion and working together. Using hot-desking to cut office space not only requires a booking system that is smart enough to seat teams together as required, and to respect any information control requirements, it requires configurable and cloud-based workstations that allows each individual to work effectively at any hot-desk. Such solutions cannot be implemented overnight.

While most of us can expect to be working in a hybrid model in the future, success in getting there is inextricably linked to the shorter-term implications of the return to the office. This is not a single-step process, or a one-size-fits-all solution. The return to the office offers many opportunities and in particular accelerating again the opportunities for people to learn and progress. In addition, employers need to be planning for the longer-term future of work and considering the benefits of a flexible model (e.g., attracting and retaining talent and/or increased cross-border or intra-team communication), while being aware of the challenges and limitations of offering complete flexibility in a hybrid model.

[1] https://www.bbc.co.uk/news/business-56319623

[2] https://www.bbc.co.uk/news/business-56319623
