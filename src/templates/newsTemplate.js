import React from "react";
import { graphql } from "gatsby";

// Import components
import Layout from "../components/layouts/layout";
import SEO from "../components/seo/seo";
import Header from "../components/header/header";
import {
  PageContainer,
  Margins,
  Gap150,
  TitleText,
  ParagraphText,
  CaseText,
  Gap60,
} from "../components/styled/UILibrary";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout>
      <SEO title="News" keywords={[`News`, `hilltop-walk`, `consulting`]} />
      <Header />
      <div className="history__container">
        <PageContainer>
          <Margins>
            <Gap150 name="news" />
            <Container style={{ objectFit: "contain" }}>
              <img src={frontmatter.image} style={{ width: "100%" }}></img>
            </Container>
            <Gap150 name="article" />
            <Row>
              <Col xs="2">
                <ParagraphText bold>By {frontmatter.author}</ParagraphText>
              </Col>
              <Col xs="2">
                <ParagraphText bold>{frontmatter.date}</ParagraphText>
              </Col>
            </Row>
            <Gap60 />
            <TitleText>{frontmatter.title}</TitleText>
            <Gap60 />
            <div dangerouslySetInnerHTML={{ __html: html }} />
          </Margins>
        </PageContainer>
      </div>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        displayDate
        slug
        title
        author
        image
      }
    }
  }
`;
