// Import dependencies
import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

// Import components
import Footer from '../footer/FooterBETA'

// Import styles
import './layout.css'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQueryBeta {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        {/* <Header siteTitle={data.site.siteMetadata.title} /> */}
        <div className='layout__container'>
          <main>{children}</main>
          <Footer />
        </div>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
