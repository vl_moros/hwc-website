// Import dependencies
import React from "react";

// Import components
import { ParagraphText } from "../styled/UILibrary";

// Import styles
import "./NewsCard.css";

const NewsCard = ({ image, title, text, preview, link, type, date, displayDate, author }) => (
  <div className="nc__container">
    <div>
      <div className="nc__image-container">
        <img src={image} style={{ width: "100%" }} />
      </div>
      <ParagraphText minSize={12} maxSize={14} style={{ margin: 20 }}>
        <br/> {type}, {displayDate}
      </ParagraphText>
      <ParagraphText minSize={18} maxSize={20} bold style={{ margin: 20 }}>
        {text}
      </ParagraphText>
      <ParagraphText minSize={12} maxSize={14} style={{ margin: 20 }}>
              By {author}
            </ParagraphText>
      <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
        {preview}
      </ParagraphText>
      <ParagraphText
        minSize={14}
        maxSize={16}
        link
        style={{ margin: 20, position: "absolute", bottom: 8 }}
      >
        {link}
      </ParagraphText>
    </div>
  </div>
);

export default NewsCard;
