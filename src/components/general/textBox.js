// Import dependencies
import React from 'react'

// Import components
import { TitleText, ParagraphText, Box } from '../styled/UILibrary'

// Import styles
import './textbox.css'


export default class TextBox extends React.Component {
    constructor(...args) {
        super(...args);
        this.state = {
            imageVisible: false,
            width: 0,
            expanded: false,
            expand_min : 0,
            expand_max: 0,
            expand_marginTop: 0

        };
        this.showImage = this.showImage.bind(this);
        this.hideImage = this.hideImage.bind(this);
        this.paragraphLong = args.paragraphLong;
        this.expand= this.expand.bind(this);
        this.shrink= this.shrink.bind(this);
    }

    showImage() {
        this.setState({ imageVisible: true })
    }
    hideImage() {
        this.setState({ imageVisible: false })
    }

    componentDidMount() {
        this.setState({ width: window.innerWidth });
    }

    expand() {
        this.setState({ expand_min: 18});
        this.setState({ expand_max: 20});
        this.setState({ expand_marginTop: 20});
        this.setState({ expanded: true});
    }

    shrink() {
        this.setState({ expand_min: 0});
        this.setState({ expand_max: 0});
        this.setState({ expand_marginTop: 0});
        this.setState({ expanded: false});
    }

    render() {
        return (
            <Box
                className='textbox__container'
                padding='8%'
                rounded
                onMouseEnter={this.state.width > 767 ? !this.props.noHover ? this.showImage : null : null}
                onMouseLeave={this.state.width > 767 ? !this.props.noHover ? this.hideImage : null : null}
                onClick={this.state.expanded ? this.shrink : this.expand}
            >
                <div>
                    <TitleText minSize={22} maxSize={30} nolineheight white={this.state.imageVisible} shadow={this.state.imageVisible}>
                        {this.props.title}
                    </TitleText>
                    <ParagraphText minSize={18} maxSize={20} style={{ marginTop: 20 }} white={this.state.imageVisible} shadow={this.state.imageVisible}>
                        {this.props.paragraph}
                    </ParagraphText>
                    <ParagraphText minSize={18} maxSize={20} style={{ marginTop: 30 }} white={this.state.imageVisible} shadow={this.state.imageVisible} link>
                        {this.state.expanded ? null : this.props.link}
                    </ParagraphText>
                     <ParagraphText minSize={this.state.expand_min} maxSize={this.state.expand_max} style={{ marginTop: this.state.expand_marginTop }} white={this.state.imageVisible} shadow={this.state.imageVisible}>
                          {this.props.paragraphLong}
                     </ParagraphText>

                    <div className='textbox__separator' style={this.state.imageVisible ? { backgroundColor: '#ffffff' } : this.props.green ? { backgroundColor: '#01D275' } : null} />
                </div>


                <img
                    alt='hover'
                    src={this.props.hoverImage}
                    className={
                        this.state.imageVisible ?
                            'textbox__hover-image textbox__hover-image-visible'
                            :
                            'textbox__hover-image'
                    }
                />
            </Box>
        );
    }
}
