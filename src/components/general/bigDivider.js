// Import dependencies
import React from "react";

// Import components
import { TitleText } from "../styled/UILibrary";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const BigDivider = ({ title }) => (
  <Container fluid style={{ margin: 0, padding: 0 }}>
    <Row>
      <Col style={{ textAlign: "center", marginTop: 10 }}>
        <hr />
      </Col>
      <Col style={{ textAlign: "center" }}>
        <TitleText>{title}</TitleText>
      </Col>
      <Col style={{ textAlign: "center", marginTop: 10 }}>
        <hr />
      </Col>
    </Row>
  </Container>
);
export default BigDivider;
