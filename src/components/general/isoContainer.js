// Import dependencies
import React from 'react'

// Import components
import { TitleText } from '../styled/UILibrary'

// Import styles
import './isocontainer.css'


const ISOContainer = ({ noMargin, isoTitle, title1, title2, image, link }) => (
    <a className='isocontainer__row' href={ link } aria-label="Values">
        <div className='isocontainer__text-container'>
            <TitleText minSize={16} maxSize={16} nolineheight white shadow>
                { isoTitle }
            </TitleText>
        </div>
    </a>
)

export default ISOContainer
