// Import dependencies
import React from "react";

// Import components
import { TitleText, SubTitleText } from "../styled/UILibrary";
import WhiteButton from "./whiteButton";
import Container from "react-bootstrap/Container";

// Import styles
import "./contactaction.css";

const ContactCallToAction = () => (
  <div className="contactaction__blue">
    <Container style={{ textAlign: "center" }}>
      <TitleText white>Let us know your goal</TitleText>
      <SubTitleText white light style={{ marginTop: 10, marginBottom: 70 }}>
        We'll help you achieve it
      </SubTitleText>
      <WhiteButton title="Contact us" destination="/contact" />
    </Container>
  </div>
);

export default ContactCallToAction;
