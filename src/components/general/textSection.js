// Import dependencies
import React from "react";

// Import components
import { SubTitleText, TitleText } from "../styled/UILibrary";

// Import styles
import "./textsection.css";

const TextSection = ({ smallTitle, contactAction, green, name }) => (
  <div className="textsection__container" name={name}>
    <SubTitleText minSize={14} maxSize={16} green={green}>
      {contactAction ? "CONTACT INFORMATION" : smallTitle}
    </SubTitleText>
    <TitleText
      minSize={22}
      maxSize={40}
      style={green ? null : { width: "70%" }}
    ></TitleText>
  </div>
);

export default TextSection;
