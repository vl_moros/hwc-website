// Import dependencies
import React from 'react'
import { Link } from 'gatsby'

// Import components
import { ParagraphText } from '../styled/UILibrary'

// Import styles
import './whiteButton.css'

const WhiteButton = ({ title, destination, margin }) => (
    <Link
        to={destination}
        className='whiteoutlinebutton__container'
        style={margin ? margin : null}
    >
        <ParagraphText minSize={14} maxSize={16} link white>
            {title}
        </ParagraphText>
    </Link>
)

export default WhiteButton
