// Import dependencies
import React from "react";

// Import components
import OutlineButton from "./outlineButton";

// Import styles
import "./contactaction.css";

const ExploreCallToAction = () => (
  <div className="contactaction__row-container">
    {/* <div>
                <TitleText minSize={22} maxSize={40}>
                    (+202) 27351005
                </TitleText>
                <ParagraphText minSize={12} maxSize={18}>
                    Spiroplastic S.A. Cairo office telephone
                </ParagraphText>
            </div> */}

    <OutlineButton title="Discover how we can help" destination="/contact" />
  </div>
);

export default ExploreCallToAction;
