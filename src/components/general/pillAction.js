// Import dependencies
import React from 'react'
import { Link } from 'gatsby'

// Import components
import { PillText } from '../styled/UILibrary'

// Import styles
import './outlinebutton.css'

const PillAction = ({ title, destination, margin, }) => (
    <Link
        to={destination}
        className='pillaction__container'
        style={margin ? margin : null}
    >
        <PillText minSize={14} maxSize={16} >
            {title}
        </PillText>
    </Link>
)

export default PillAction