import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const Fmsb = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(relativePath: { eq: "fmsb.png" }) {
          childImageSharp {
            fluid(maxWidth: 300) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={(data) => (
      <Img
        fluid={data.placeholderImage.childImageSharp.fluid}
        objectFit="cover"
      />
    )}
  />
);
export default Fmsb;
