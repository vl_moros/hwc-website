// Import dependencies
import React from "react";
import { Link } from "gatsby";
import { PageContainer, Margins, TitleText } from "../styled/UILibrary";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "bootstrap/dist/css/bootstrap.css";
import "./header.css";
import "../../pages/styles/home.css";

// Import icons
import Logo from "../../assets/icons/home-page-logo.svg";
import Menu from "../../assets/icons/menu.svg";
import Cross from "../../assets/icons/cross.svg";
import External_link from "../../assets/icons/external_link.svg";

export default class HomeHeader extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = { toggled: "" };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.state.toggled === ""
      ? this.setState({ toggled: " header__menu-toggled" })
      : this.setState({ toggled: "" });
  }

  render() {
    return (
      <div className="header__container">
        <PageContainer margin={"10px"}>
          <Margins style={{ width: "100%" }}>
            <div className="header__container-inner">
              <div className="header__logo-container">
                <a href="/">
                  <Logo className="header__logo" />
                </a>
                <div className="header__links-container">
                  <Link
                    to="/services"
                    className={"header__link header__link-black"}
                  >
                    What we do
                  </Link>
                  <Link
                    to="/insights"
                    className={"header__link header__link-black"}
                  >
                    Insights
                  </Link>
                  <Link
                    to="/news"
                    className={"header__link header__link-black"}
                  >
                    News
                  </Link>
                  <Link
                    to="/about"
                    className={"header__link header__link-black"}
                  >
                    About
                  </Link>
                  <Link
                    to="/contact"
                    className={"header__link header__link-black"}
                  >
                    Contact
                  </Link>

                  <a
                    href="https://hilltopwalk.zohorecruit.eu/jobs/Careers"
                    target="_blank"
                    className={"header__link header__link-black"}
                  >
                    Careers
                    <External_link className="external__logo"/>
                  </a>

                </div>

                {/* Burger button */}
                <div
                  className="header__burger-container"
                  onClick={this.toggleMenu}
                >
                  <Menu className="header__burger-menu" />
                </div>

                <div
                  className={
                    "header__mobile-menu-container" + this.state.toggled
                  }
                >
                  {/* Burger button */}
                  <div
                    className="header__burger-container"
                    onClick={this.toggleMenu}
                  >
                    <Cross className="header__burger-menu" />
                  </div>

                  <div style={{ width: "100%", height: 20 }} />
                  <Container fluid style={{ margin: 0, padding: 0 }}>
                    <Row>
                      <Col
                        xl={6}
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        className="header__column-padding text-center"
                      >
                        <Link
                          to="/"
                          className="header__mobile-title header__mobile-button"
                          style={
                            this.props.recycled
                              ? { color: "#01D275", fontSize: 16 }
                              : { color: "#000000", fontSize: 16 }
                          }
                        >
                          HOME
                        </Link>
                      </Col>
                      <Col
                        xl={6}
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        className="header__column-padding text-center"
                      >
                        <Link
                          to="/services"
                          className="header__mobile-title header__mobile-button"
                          style={
                            this.props.recycled
                              ? { color: "#01D275", fontSize: 16 }
                              : { color: "#000000", fontSize: 16 }
                          }
                        >
                          WHAT WE DO
                        </Link>
                      </Col>
                      <Col
                        xl={6}
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        className="header__column-padding text-center"
                      >
                        <Link
                          to="/news"
                          className="header__mobile-title header__mobile-button"
                          style={
                            this.props.recycled
                              ? { color: "#01D275", fontSize: 16 }
                              : { color: "#000000", fontSize: 16 }
                          }
                        >
                          NEWS
                        </Link>
                      </Col>
                      <Col
                        xl={6}
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        className="header__column-padding text-center"
                      >
                        <Link
                          to="/about"
                          className="header__mobile-title header__mobile-button"
                          style={
                            this.props.recycled
                              ? { color: "#01D275", fontSize: 16 }
                              : { color: "#000000", fontSize: 16 }
                          }
                        >
                          ABOUT
                        </Link>
                      </Col>
                      <Col
                        xl={6}
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        className="header__column-padding text-center"
                      >
                        <Link
                          to="/contact"
                          className="header__mobile-title header__mobile-button"
                          style={
                            this.props.recycled
                              ? { color: "#01D275", fontSize: 16 }
                              : { color: "#000000", fontSize: 16 }
                          }
                        >
                          CONTACT
                        </Link>
                      </Col>
                       <Col
                                              xl={6}
                                              lg={6}
                                              md={6}
                                              sm={6}
                                              xs={6}
                                              className="header__column-padding text-center"
                                            >
                                              <Link
                                                to="https://hilltopwalk.zohorecruit.eu/jobs/Careers"
                                                className="header__mobile-title header__mobile-button"
                                                style={
                                                  this.props.recycled
                                                    ? { color: "#01D275", fontSize: 16 }
                                                    : { color: "#000000", fontSize: 16 }
                                                }
                                              >
                                                CAREERS
                                              </Link>
                                            </Col>

                    </Row>
                  </Container>
                </div>
              </div>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
