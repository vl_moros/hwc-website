// Import dependencies
import React from "react";

import {
  TitleText,
  PageContainer,
  ParagraphText,
  Margins,
  Gap60,
  Gap150,
} from "../styled/UILibrary";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

// Import styles
import "./footer.css";
import Logo from "../../assets/icons/home-page-logo.svg";

const Footer = () => (
  <PageContainer margin={"0"}>
    <Margins>
      <Gap60 />

      <div className="footer__bottom-bar">
        <div className="footer__bottom-bar-left">
          <ParagraphText minSize={12} maxSize={16}>
            ©2021 Hilltop Walk Consulting Limited
          </ParagraphText>
        </div>
        {/* <div className="footer__rootnode-container">
          <ParagraphText minSize={12} maxSize={16}>
            Designed & developed by&nbsp;
          </ParagraphText>
          <a href="https://rootnode.io/">
            <ParagraphText minSize={12} maxSize={16}>
              rootnode
            </ParagraphText>
          </a>
        </div> */}
      </div>

      <Gap60 />
    </Margins>
  </PageContainer>
);

export default Footer;


