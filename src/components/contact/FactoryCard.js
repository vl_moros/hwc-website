// Import dependencies
import React from "react";

// Import components
import { Box, ParagraphText } from "../styled/UILibrary";

// Import styles
import "./factorycard.css";

const FactoryCard = ({ image, title, text, preview }) => (
  <Box className="fc__container" padding="20px">
    <div>
      <div className="fc__image-container">{image}</div>
      <ParagraphText minSize={12} maxSize={14} link style={{ marginTop: 10 }}>
        {title}
      </ParagraphText>
      <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
        {text}
      </ParagraphText>
      <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
        {preview}
      </ParagraphText>
      <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
        <a href="/news" style={{ textDecoration: "none" }}>
          Learn more
        </a>
      </ParagraphText>
    </div>
  </Box>
);

export default FactoryCard;
