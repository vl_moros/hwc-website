// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
  Gap60,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheader.css";
import "../header/header.css";
import "../footer/footer.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheader__container">
        <PageContainer margin={"40px"}>
          <Margins>
            <div className="homeheader__content-container">
              <div className="homeheader__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  CONDUCT
                </ParagraphText>
                <TitleText minSize={30} maxSize={45} white shadow nolineheight>
                  Practical approaches to improving conduct and client outcomes, and managing potential conflicts of interest

                </TitleText>
              </div>
            </div>
            <div style={{ width: 1000, position: "absolute", bottom: 100 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    CONDUCT
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    EXPERTISE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLUTIONS
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
