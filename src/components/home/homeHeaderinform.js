// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheader.css";
import "../header/header.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheader__container">
        <PageContainer margin={"10px"}>
          <Margins>
            <div className="homeheader__content-container">
              <div className="homeheader__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  EXPERTISE
                </ParagraphText>
                <TitleText minSize={30} maxSize={45} white shadow nolineheight>
                  Relevant experience with at the forefront of the changes in financial markets over the past two decades
                </TitleText>
              </div>
            </div>
            <div style={{ width: 1000, position: "absolute", bottom: 100 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    CONDUCT
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    EXPERTISE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLUTIONS
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
