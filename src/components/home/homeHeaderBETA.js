// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
  Gap60,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheaderBETA.css";
import "../header/headerBETA.css";
import "../footer/footer.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheaderBETA__container">
        <PageContainer margin={"40px"}>
          <Margins>
            <div className="homeheaderBETA__content-container">
              <div className="homeheaderBETA__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  PARTNER
                </ParagraphText>
                <TitleText minSize={40} maxSize={65} white shadow nolineheight>
                  Working with you <br /> to grow your business
                </TitleText>
              </div>
            </div>
            <div style={{ width: 1000, position: "absolute", bottom: 100 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    PARTNER
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLVE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    INFORM
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
