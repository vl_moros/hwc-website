// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheader.css";
import "../header/header.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheader__container">
        <PageContainer margin={"0px"}>
          <Margins>
            <div className="homeheader__content-container">
              <div className="homeheader__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  SOLUTIONS
                </ParagraphText>
                <TitleText minSize={30} maxSize={45} white shadow nolineheight>
                  Offer practical solutions via guidance, training and leveraging the right processes and technology
                </TitleText>
              </div>
            </div>
            <div style={{ width: 100, position: "absolute", bottom: 10 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    CONDUCT
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    EXPERTISE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLUTIONS
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
