// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheaderBETA.css";
import "../header/headerBETA.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheaderBETA__container">
        <PageContainer margin={"10px"}>
          <Margins>
            <div className="homeheaderBETA__content-container">
              <div className="homeheaderBETA__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  INFORM
                </ParagraphText>
                <TitleText minSize={40} maxSize={65} white shadow nolineheight>
                  Turning your data <br /> into usable information
                </TitleText>
              </div>
            </div>
            <div style={{ width: 1000, position: "absolute", bottom: 100 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    PARTNER
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLVE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    INFORM
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
