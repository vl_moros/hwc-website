// Import dependencies
import React from "react";

// Import components
import {
  PageContainer,
  Margins,
  TitleText,
  ParagraphText,
} from "../styled/UILibrary";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./homeheaderBETA.css";
import "../header/headerBETA.css";

export default class HomeHeader extends React.Component {
  render() {
    return (
      <div className="homeheaderBETA__container">
        <PageContainer margin={"0px"}>
          <Margins>
            <div className="homeheaderBETA__content-container">
              <div className="homeheaderBETA__navbar-container"></div>
              <div>
                <ParagraphText white shadow nolineheight bold>
                  SOLVE
                </ParagraphText>
                <TitleText minSize={40} maxSize={65} white shadow nolineheight>
                  Helping you to <br /> find the answers
                </TitleText>
              </div>
            </div>
            <div style={{ width: 100, position: "absolute", bottom: 10 }}>
              <Row>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    PARTNER
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    SOLVE
                  </ParagraphText>
                </Col>
                <Col>
                  <ParagraphText white shadow nolineheight bold>
                    INFORM
                  </ParagraphText>
                </Col>
              </Row>
            </div>
          </Margins>
        </PageContainer>
      </div>
    );
  }
}
