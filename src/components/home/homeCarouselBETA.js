// Import dependencies
import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";
import HomeHeader from "./homeHeaderBETA";
import HomeHeadersolve from "./homeHeadersolveBETA";
import HomeHeaderinform from "./homeHeaderinformBETA";

// Import styles
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./homecarouselBETA.css";

// Import images
import solve from "../../assets/images/solve.jpg";
import partner from "../../assets/images/partner.jpg";
import inform from "../../assets/images/inform.jpg";

export default class HomeCarousel extends Component {
  render() {
    return (
      <Carousel
        width="100vw"
        autoPlay
        infiniteLoop
        interval={8000}
        transitionTime={700}
        showArrows={true}
        showStatus={false}
        showThumbs={false}
        showIndicators={false}
        stopOnHover={false}
        swipeable={false}
      >
        <div>
          <div style={{ textAlign: "left" }}>
            <HomeHeader />
          </div>
          <img
            src={partner}
            className="homecarouselBETA__image-container"
            alt="partners"
          />
        </div>
        <div>
          <div style={{ textAlign: "left" }}>
            <HomeHeadersolve />
          </div>
          <img
            src={solve}
            className="homecarouselBETA__image-container"
            alt="solve"
          />
        </div>
        <div>
          <div style={{ textAlign: "left" }}>
            <HomeHeaderinform />
          </div>
          <img
            src={inform}
            className="homecarouselBETA__image-container"
            alt="inform"
          />
        </div>
      </Carousel>
    );
  }
}
