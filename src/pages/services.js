// Import dependencies
import React from "react"

// Import components
import Layout from "../components/layouts/layout"
import SEO from "../components/seo/seo"
import { PageContainer, Margins, Gap150, TitleText, ParagraphText, CaseText, Gap60, Gap200} from "../components/styled/UILibrary"
import ContactCallToAction from "../components/general/contactAction"
import Header from "../components/header/header"
import BigDivider from "../components/general/bigDivider"
import TextBox from "../components/general/textBox"

// Bootstrap components
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

// Import styles
import "bootstrap/dist/css/bootstrap.css"
import "./styles/home.css"
import "./styles/history.css"
import "./styles/scrollbar.css"

// Import images
import Blue from '../assets/images/blue.png'
import Together from "../assets/images/Together.jpeg";


export default class ServicesPage extends React.Component {

  componentDidMount() {

    // Hello for developers
    console.clear()
    console.log(
      "%cDesigned & developed by Vladi Moros",
      "background: #0000FF; color: #ffffff; font-size: 12px; padding: 25px; font-family: 'Montserrat', sans-serif; line-height: 22px"
    )
  }

  render() {
    return (
      <Layout>
        <SEO title="Services" keywords={[`Services`, `consulting`, `London`]} />
        <Header />
        <div className="history__container">
        <PageContainer>
            <Margins>
                <Gap60 name="What We Do" />
                <BigDivider title="What we do" />
                <Gap60/>
                <div>
                   <img
                    className="box__image-container"
                    src={Together}
                    alt="Hilltop Walk"
                   />
                   <Gap60/>
                </div>
                <div>
                    <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 40 }} style={{ marginBottom: 35 }}>
                        Hilltop Walk Consulting delivers innovative solutions to clients, and helps develop procedures and market guidelines to
                        navigate changing market conditions and raise the standards of conduct among all market participants.
                    </ParagraphText>
                </div>
                <div style={{ width: "100%", height: 20 }} />
                <Container
                    fluid
                    style={{ margin: 0, padding: 0 }}
                    className="services"
                >
                <Row>
                  <Col
                    xl={6}
                    lg={6}
                    md={6}
                    sm={12}
                    xs={12}
                    className="home__column-padding"
                  >
                    <TextBox
                      title="Conduct"
                      paragraph="Effectively managing conflicts of interest to ensure fair client outcomes."
                      paragraphLong = " The expectations on market-makers have changed over the past 10 years from an expectation of participating in efficient markets to one where market-makers have to be able to demonstrate that they are operating fairly.
                      This means that market-makers need to be transparent with their clients on how they operate and have effective processes in place to identify and manage any conflicts of interest between themselves and their clients and between
                      different clients. These processes can’t be prescriptive rules as the nature of OTC markets mean that there is no prescribed way that two market participants will interact with each other.
                      We work with our clients following the same approach as the FX Global Code and FMSB standards in developing practitioner led principles that best fit each underlying business and are complemented with interactive scenario based
                      training sessions to ensure that each individual understands what is expected from them."
                      link="Learn more"
                      hoverImage={Blue}
                      
                    />
                  </Col>
                  <Col
                    xl={6}
                    lg={6}
                    md={6}
                    sm={12}
                    xs={12}
                    className="home__column-padding"
                  >
                    <TextBox
                      title="Strategy"
                      paragraph="Identifying and executing the right growth opportunities for each business."
                      paragraphLong = "Markets businesses have grown ever more complicated through the progress of automation and regulation over the past ten years. Identifying the right growth opportunities for an individual business is not straightforward.
                      We work with our clients to identify their individual strengths and challenges to shape their business strategy in order to play to those strengths and mitigate those weaknesses.
                      We do this by building deep partnerships with our clients so that we understand them and what differentiates them from their competitors.
                      With a good understanding of strengths and weaknesses and a clear plan and organisation around a growing business, growth can be genuinely additive and innovative to both a markets business and its clients."
                      link="Learn more"
                      hoverImage={Blue}
                      
                    />
                  </Col>
                  <Col
                    xl={6}
                    lg={6}
                    md={6}
                    sm={12}
                    xs={12}
                    className="home__column-padding"
                  >
                    <TextBox
                      title="Change"
                      paragraph="Delivering change through understanding, partnership, and strategic considerations."
                      paragraphLong="Markets businesses need to change continuously as standing still will mean going backwards in comparison to your competitors. However, the complexity of markets businesses means that change can have unintended consequences both for the markets business and for its clients.
                      Successful change requires the combination of a clear vision and strategy about what you are trying to achieve, and the necessary understanding and buy-in from all functions of the business.
                      At Hilltop Walk Consulting, we work with our clients to both help them crisply articulate what they are trying to achieve to all relevant stakeholders internally and externally and also to gather the necessary information from those stakeholders to ensure that accurate appropriate decisions are taken at every step through a change programme."
                      link="Learn more"
                      hoverImage={Blue}
                      
                    />
                  </Col>
                  <Col
                    xl={6}
                    lg={6}
                    md={6}
                    sm={12}
                    xs={12}
                    className="home__column-padding"
                  >
                    <TextBox
                      title="Technology"
                      paragraph="Technology solutions that effectively and efficiently deliver business requirements."
                      paragraphLong="The importance of high quality technology solutions in financial markets is ever increasing. However, the complexity of the processes that are being automated increases and therefore so does the importance of clear communication and understanding from both business users and technology teams to avoid situations where the solution doesn’t enable the necessary business outcomes and there is disappointment on both sides.
                      Better results can be achieved by business users spending more time and energy understanding what tasks and process can easily be automated and what makes automation more difficult, and technology teams understanding and focusing on the business problem that they are trying to address.
                      At Hilltop Walk Consulting, we have experience both as a business user and delivering technology which allows us to help facilitate the constructive dialogue and partnership that allows new technology to be delivered in such a way that the business outcomes are achieved seamlessly and effectively."
                      link="Learn more"
                      hoverImage={Blue}
                      
                    />
                  </Col>
                </Row>
              </Container>
              <Gap200/>
            </Margins>
          </PageContainer>
          </div>

        <ContactCallToAction />
      </Layout>
    )
  }
}