// Import dependencies
import React from "react";

// Import components
import Layout from "../components/layouts/layoutBETA";
import SEO from "../components/seo/seo";
import {
  PageContainer,
  Margins,
  Gap150,
  Gap60,
  TitleText,
  ParagraphText,
  CaseText,
} from "../components/styled/UILibrary";
import TextSection from "../components/general/textSection";
import ContactCallToAction from "../components/general/contactAction";
import HomeCarousel from "../components/home/homeCarouselBETA";
import Header from "../components/header/headerBETA";
import PillAction from "../components/general/pillAction";
import OutlineButton from "../components/general/outlineButton";
import BigDivider from "../components/general/bigDivider";
import FactoryCard from "../components/contact/FactoryCard";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import images
import who_we_are from "../../public/images/HTW2.png";
import who_we_are2 from "../../public/images/HTW4.png";
import who_we_are3 from "../../public/images/HTW5.png";
import fmsb_image from "../../public/images/FMSB1.png";
import hwc_image from "../../public/images/HTW7.png";


import our_proud_achievements from "../assets/images/our-proud-achievements.png";
import Fmsb from "../components/images/fmsb";
import Hwc from "../components/images/hwc";

// Import styles
import "bootstrap/dist/css/bootstrap.css";
import "./styles/home.css";
import "./styles/history.css";
import "./styles/scrollbar.css";

// Load lodash
var _ = require("lodash");

export default class IndexPage extends React.Component {
  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`Hilltop`, `Walk`, `Consulting`]} />
        <Header />
        <div className="history__container">
          {/* <HeaderImage /> */}
          <HomeCarousel />

          {/* Page contents */}
          <PageContainer margin={"0"}>
            <Margins>
              <Gap60 name="who-we-are" />

              <Container
                fluid
                style={{ margin: 0, padding: 0 }}
                className="home__equal"
              >
                <Row style={{ width: "100%" }}>
                 <TitleText>
                    We help your business unlock its full potential.
                    <br/><br/>
                 </TitleText>
                    <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 40 }} style={{ marginBottom: 35 }}>
                        Hilltop Walk Consulting is a new financial markets consultancy firm specialising in market conduct, strategy,
                        technology, and change.
                   </ParagraphText>

                    <OutlineButton
                      title="Discover how we can help"
                      destination="/servicesBETA"
                    />
                </Row>
              </Container>

              <Gap150 name="our-proud-achievements" />
              <BigDivider title="Our latest insights" />
              <Container fluid style={{ margin: 0, padding: 0 }}>
                <Row>
                    <Col xl={6} lg={6} md={6} sm={12} xs={12}>
                        <ParagraphText bold style={{ marginTop: 35 }}>
                          <a href="/conduct-insight-paper">
                          <img
                            style={{ maxWidth: "100%" }}
                            src={who_we_are}
                            alt="Who We Are"
                            />
                            </a>
                            <br/><br/>
                          </ParagraphText>
                          <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
                            Conduct Insight Paper by Allan Guild
                           </ParagraphText>
                         <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
                            Placeholder for a short preview on the White paper
                         </ParagraphText>
                        <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
                           <a href="/conduct-insight-paper" style={{ textDecoration: "none" }}>Read report</a>
                       </ParagraphText>
                    </Col>
                    <Col xl={6} lg={6} md={6} sm={12} xs={12}>
                     <ParagraphText bold style={{ marginTop: 35 }}>
                     <a href="/sa-ccr-insight-paper">
                     <img
                            style={{ maxWidth: "100%" }}
                            src={who_we_are2}
                            alt="Who We Are"
                     />
                     </a>
                        <br/><br/>
                        </ParagraphText>
                        <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
                        SA-CCR Insight Paper by Vladi Moros
                        <br/>
                         </ParagraphText>
                          <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
                            Placeholder for a short preview on the White paper
                         </ParagraphText>
                        <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
                        <a href="/sa-ccr-insight-paper" style={{ textDecoration: "none" }}>Read report</a>
                        </ParagraphText>
                      </Col>

                       <Col xl={6} lg={6} md={6} sm={12} xs={12}>
                       <ParagraphText bold style={{ marginTop: 35 }}>
                       <a href="/compression-insight-paper">
                       <img
                        style={{ maxWidth: "100%" }}
                        src={who_we_are3}
                        alt="Who We Are"
                       />
                       </a>
                       <br/><br/>
                       </ParagraphText>
                       <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
                       Compression Insight Paper by Vladi Moros
                       <br/>
                       </ParagraphText>
                        <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
                                                   Placeholder for a short preview on the White paper
                                                </ParagraphText>
                       <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
                       <a href="/compression-insight-paper" style={{ textDecoration: "none" }}>Read report</a>
                        </ParagraphText>
                       </Col>
                </Row>
              </Container>

             <Gap150 name="our-proud-achievements" />
              <BigDivider title="Our latest news" />
              <Container fluid style={{ margin: 0, padding: 0 }}>
                <Row>
                    <Col xl={6} lg={6} md={6} sm={12} xs={12}>
                        <ParagraphText bold style={{ marginTop: 35 }}>
                          <a href="/hilltop-walk-consulting">
                          <img
                            style={{ maxWidth: "100%" }}
                            src={hwc_image}
                            alt="Who We Are"
                            />
                        </a>
                        <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
                            Hilltop Walk Consulting.
                        </ParagraphText>
                        <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
                           I am delighted to be able to share that today, Hilltop Walk Consulting has opened its
                           doors for business.
                        </ParagraphText>
                        <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
                            <a href="/hilltop-walk-consulting" style={{ textDecoration: "none" }}>Learn more</a>
                        </ParagraphText>
                       </ParagraphText>
                    </Col>

                    <Col xl={6} lg={6} md={6} sm={12} xs={12}>
                     <ParagraphText bold style={{ marginTop: 35 }}>
                     <a href="/Hilltop-Walk-Consulting-Confirms-FICC-Markets-Standards-Board-as-a-Client">
                     <img
                            style={{ maxWidth: "100%" }}
                            src={fmsb_image}
                            alt="Who We Are"
                     />
                     </a>
                    <ParagraphText minSize={18} maxSize={20} bold style={{ marginTop: 10 }}>
                                               Hilltop Walk Consulting confirms FICC Markets Standard Board as a Client.
                                           </ParagraphText>
                                           <ParagraphText minSize={16} maxSize={18} style={{ marginTop: 10 }}>
                                               Hilltop Walk Consulting will be working with FMSB and its members to develop market
                                               Standards, Statements of Good Practice and Spotlight Reviews, in support of FMSB’s
                                               mission to ensure that wholesale FICC markets are transparent,
                                               fair and effective for all participants.
                                           </ParagraphText>
                                           <ParagraphText minSize={14} maxSize={16} link style={{ marginTop: 35 }}>
                                               <a href="/Hilltop-Walk-Consulting-Confirms-FICC-Markets-Standards-Board-as-a-Client" style={{ textDecoration: "none" }}>Learn more</a>
                               </ParagraphText>
                               </ParagraphText>
                      </Col>
                </Row>
              </Container>

            </Margins>
          </PageContainer>
        </div>
        <Gap60 />
        <ContactCallToAction />
      </Layout>
    );
  }
}
