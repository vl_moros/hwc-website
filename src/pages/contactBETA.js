// Import dependencies
import React from "react";

// Import components
import Layout from "../components/layouts/layoutBETA";
import SEO from "../components/seo/seo";
import Header from "../components/header/headerBETA";
import ContactForm from "../components/contact/ContactForm";
import {
  PageContainer,
  Margins,
  Gap150,
  Gap60,
} from "../components/styled/UILibrary";
import TitleSection from "../components/general/titleSection";
import BigDivider from "../components/general/bigDivider";

// Import styles
import "./styles/contact.css";
import "./styles/history.css";
import "./styles/scrollbar.css";
import "bootstrap/dist/css/bootstrap.css";

export default class ContactPage extends React.Component {
  componentDidMount() {
    // Hello for developers
    console.clear();
    console.log(
      "%cDesigned & developed by Vladi Moros",
      "background: #0000FF; color: #ffffff; font-size: 12px; padding: 25px; font-family: 'Montserrat', sans-serif; line-height: 22px"
    );
  }

  render() {
    return (
      <Layout>
        <SEO title="Contact" />
        <Header />

        <div className="history__container">
          <PageContainer>
            <Margins>
              <Gap60 name="reach-us" />

              <BigDivider title="Contact us" />

              <Gap60 name="contact-form" />
              <TitleSection smallTitle="Let us know how we can help and we will get back to you" />
              <Gap60 />
              <ContactForm />
              <Gap150 />
            </Margins>
          </PageContainer>
        </div>
      </Layout>
    );
  }
}
