// Import dependencies
import React from "react";
import { graphql } from "gatsby";

// Import components
import Layout from "../components/layouts/layoutBETA";
import SEO from "../components/seo/seo";
import Header from "../components/header/headerBETA";
import ContactCallToAction from "../components/general/contactAction";
import {
  PageContainer,
  Margins,
  Gap200,
  Gap150,
  Gap60,
} from "../components/styled/UILibrary";
import BigDivider from "../components/general/bigDivider";
import NewsCard from "../components/news/NewsCard";
import "../components/news/NewsCard.css";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import styles
import "./styles/history.css";
import "./styles/scrollbar.css";

export default class InsightsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      insights: [],
    };
  }

  componentDidMount() {
    // Hello for developers
    this.setState({ insights: this.props.data.allMarkdownRemark.edges });
    console.clear();
    console.log(this.state);
  }

  render() {
    return (
      <Layout>
        <SEO title="Insights" keywords={[`Insights`, `hilltop-walk`, `consulting`]} />
        <Header />
        <div className="history__container">
          <PageContainer>
            <Margins>
              <Gap60 name="Insights" />
              <BigDivider title="Insights" />
              <Gap60 />

              <Container fluid style={{ margin: 0, padding: 0 }}>
                <Row>
                  {this.state.insights.map((item) => {
                    const { title, image, slug, date, type, displayDate, author } = item.node.frontmatter;
                    return (
                      <Col xs="auto">
                        <NewsCard
                          image={image}
                          text={title}
                          link={<a href={slug}>Learn More</a>}
                          displayDate = {displayDate}
                          date = {date}
                          type= {type}
                          author = {author}
                        />
                      </Col>
                    );
                  })}
                </Row>
              </Container>
              <Gap200 />
            </Margins>
          </PageContainer>
        </div>
        <ContactCallToAction />
      </Layout>
    );
  }
}

export const pageQuery = graphql`
  query InsightsBetaQuery {
    allMarkdownRemark(filter: {frontmatter: {type: {eq: "WhitePaper"}}}) {
      edges {
        node {
          frontmatter {
            title
            date
            displayDate
            slug
            image
            type
            author
          }
        }
      }
    }
  }
`;

