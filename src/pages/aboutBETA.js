// Import dependencies
import React from "react";

// Import components
import Layout from "../components/layouts/layoutBETA";
import SEO from "../components/seo/seo";
import Header from "../components/header/headerBETA";
import {
  PageContainer,
  Margins,
  Gap60,
  Gap150,
  Gap200,
  TitleText,
  ParagraphText,
} from "../components/styled/UILibrary";
import ContactCallToAction from "../components/general/contactAction";
import BigDivider from "../components/general/bigDivider";
import TitleSection from "../components/general/titleSection";
import OutlineButton from "../components/general/outlineButton";

// Import styles
import "../components/general/outlinebutton.css";
import "./styles/history.css";
import "./styles/about.css";
import "./styles/scrollbar.css";

// Bootstrap components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import images
import Allan from "../assets/images/AllanD.png";
import Vladi from "../assets/images/VladiD.png";
import Gary from "../assets/images/GaryD.png";
import Together from "../assets/images/Together.jpeg";

export default class AboutPage extends React.Component {
  componentDidMount() {
    // Hello for developers
    console.clear();
    console.log(
      "%cDesigned & developed by Vladi Moros",
      "background: #0000FF; color: #ffffff; font-size: 12px; padding: 25px; font-family: 'Montserrat', sans-serif; line-height: 22px"
    );
  }

  render() {
    return (
      <Layout>
        <SEO
          title="About"
          keywords={[`about`, `Hilltop Walk Consulting`, `London`]}
        />

        <div className="history__container">
          <Header />
          {/* Page contents */}
          <PageContainer>
            <Margins>
              <Gap60 />
                <BigDivider title="About us" />
                <Gap60 />
                <div class="cards">
                    <div class="cardSmall">
                        <TitleText>Who we are</TitleText>
                        <br/>
                            Hilltop Walk Consulting is a new financial markets consultancy firm
                            specialising in market conduct, strategy,technology, and change.
                            We have over 15 years of experience on leading Technology, Operations and Business Management
                            Teams, Electronic Trading and Change programs responding to some of the biggest market challenges
                            including Dodd-Frank, EMIR, Vocker and Brexit.
                    </div>
                    <div class="cardSmall">
                        <TitleText>Our Approach</TitleText>
                        <br/>
                            Our culture is a  combination of hands-on development and forging relationships with key stakeholders.
                            Our focus is on the specific needs of the client for both technology and change management.
                            With a strong background of architecting and delivering advanced IT solutions in Banking,
                            we avoid re-inventing the wheel and prefer a pragmatic approach.
                    </div>

                  <div class="card">
                    <Gap60 />
                    <img src={Allan} alt="Allan Guild" />
                  </div>
                  <div class="card">
                    <Gap60 />
                    <TitleText blue style={{ marginBottom: 20 }}>
                              Allan Guild
                    </TitleText>
                    <TitleSection smallTitle="Director" />
                        <ParagraphText style={{ marginBottom: 20 }}>
                              Allan founded Hilltop Walk Consulting after
                              a long career in Financial Markets at HSBC and
                              Goldman Sachs. In that time, Allan worked in a range of roles
                              across all aspects of the business including leading Technology, Operations and
                              Business Management teams, Electronic Trading businesses
                              and Change programs responding to some of the biggest
                              regulatory changes in Markets including Dodd-Frank, EMIR,
                              Volcker and Brexit. Allan has also served as chair of the
                              Global FX Division’s Steering Committee and played a
                              leading role in the formation of LCH ForexClear, the
                              leading FX Clearing House.
                        </ParagraphText>
                  </div>

                  <div class="card">
                    <Gap60 />
                    <img src={Vladi} alt="Vladi Moros"/>
                  </div>
                  <div class="card">
                    <Gap60 />
                    <TitleText blue style={{ marginBottom: 20 }}>
                            Vladi Moros
                    </TitleText>
                    <TitleSection smallTitle="Assosiate" />
                    <ParagraphText style={{ marginBottom: 20 }}>
                            Vladi is a consultant specializing in FICC markets regulatory change programs placed
                            within a top-tier investment bank.  Part of his engagement includes helping the COO team
                            with the review and production of updated procedures for compression and optimization, including
                            the process and system improvement suggestions.
                    </ParagraphText>
                  </div>

                  <div class="card">
                    <Gap60 />
                    <img src={Gary} alt="Gary Garyfalos" />
                  </div>
                  <div class="card">
                    <Gap60 />
                    <TitleText blue style={{ marginBottom: 20 }}>
                        Anargyros (Gary) Garyfalos
                    </TitleText>
                    <TitleSection smallTitle="Consultant" />
                    <ParagraphText style={{ marginBottom: 20 }}>
                        Gary brings over 15 years of experience designing and delivering advanced
                        technology systems in the financial sector. Originally from a telecoms background, Gary has since
                        worked in investment banks such as Goldman Sachs, JP Morgan & HSBC. In these organisation, Gary
                        lead development in the areas of FX e-Trading, Equity and Commodities
                        Risk Management,  Trade Reporting and firm wide transformations for regulatory demands (MiFID).
                    </ParagraphText>
                  </div>
            </div>
              <Gap200 />
              <ContactCallToAction />
            </Margins>
          </PageContainer>
        </div>
      </Layout>
    );
 }
}

